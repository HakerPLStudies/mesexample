﻿using MesExample.Model;

namespace MesExample.CalculationPLocal
{
    public interface ICalculationPLocal
    {
        void Calculate(Grid grid, GlobalData globalData);
    }
}
