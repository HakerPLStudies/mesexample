﻿using MesExample.Model;

namespace MesExample.Agregation
{
    public interface IAgregation
    {
        void Agregate(Soe soe, Grid grid);
    }
}
