﻿using System.Collections.Generic;
using MesExample.Helper;
using MesExample.Model;

namespace MesExample.ViewModel.Command.MesViewViewModel
{
    public class BuildGridCommand : ICommand
    {
        private readonly ViewModel.MesViewViewModel _viewModel;
        public BuildGridCommand(ViewModel.MesViewViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public void Execute(object sender)
        {
            if (_viewModel.GlobalData == null)
                return;

            BuildElementList();
            BuildNodeList();
        }

        private void BuildElementList()
        {
            double positionX = _viewModel.GlobalData.L / (_viewModel.GlobalData.NodeNumbers - 1);

            Node first = BuildNode(0, 0 * positionX);
            for (int i = 0, nodeId = 1; i < _viewModel.GlobalData.NodeNumbers - 1; i++, nodeId++)
            {
                Element element = new Element(2);
                element.Id = i;

                Node secound = BuildNode(nodeId, nodeId * positionX);

                element.Nodes.Add(first);
                element.Nodes.Add(secound);

                first = secound;

                _viewModel.Grid.Elements.Add(element);
            }
        }

        private Node BuildNode(int index, double position)
        {
            Node node = new Node()
            {
                Id = index,
                PositionX = position,
                Bc = BoundaryConditions.None,
                PLocal = 0
            };

            return node;
        }

        private void BuildNodeList()
        {
            foreach (Element element in _viewModel.Grid.Elements)
            {
                foreach (Node node in element.Nodes)
                {
                    if (!_viewModel.Grid.Nodes.Contains(node))
                        _viewModel.Grid.Nodes.Add(node);
                }
            }
        }
    }
}
