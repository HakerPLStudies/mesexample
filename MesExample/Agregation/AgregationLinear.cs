﻿using System;
using MesExample.Model;

namespace MesExample.Agregation
{
    public class AgregationLinear : IAgregation
    {
        public void Agregate(Soe soe, Grid grid)
        {
            if (soe == null || grid == null)
                return;

            AgregateHGlobal(soe, grid);
            AgregatePGlobal(soe, grid);
        }

        private void AgregateHGlobal(Soe soe, Grid grid)
        {
            foreach (Element element in grid.Elements)
            {
                soe.HGlobal[element.Nodes[0].Id][element.Nodes[0].Id] += element.HLocal[0][0];
                soe.HGlobal[element.Nodes[0].Id][element.Nodes[1].Id] += element.HLocal[0][1];
                soe.HGlobal[element.Nodes[1].Id][element.Nodes[0].Id] += element.HLocal[1][0];
                soe.HGlobal[element.Nodes[1].Id][element.Nodes[1].Id] += element.HLocal[1][1];
            }
        }

        private void AgregatePGlobal(Soe soe, Grid grid)
        {
            foreach (Node node in grid.Nodes)
                soe.PGlobal[node.Id] = node.PLocal;
        }
    }
}
