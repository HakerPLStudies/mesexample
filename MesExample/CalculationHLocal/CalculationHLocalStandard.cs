﻿using MesExample.Model;
using System.Linq;

namespace MesExample.CalculationHLocal
{
    public class CalculationHLocalStandard : ICalculationHLocal
    {
        public void Calculate(Grid grid, GlobalData globalData)
        {
            if (globalData == null)
                return;

            foreach (Element element in grid.Elements)
            {
                double elementL = LenghtBetweenNodeInElement(element);

                element.HLocal[0][0] = CalculateC(globalData.S, globalData.K, elementL);
                element.HLocal[0][1] = -CalculateC(globalData.S, globalData.K, elementL);
                element.HLocal[1][0] = -CalculateC(globalData.S, globalData.K, elementL);
                element.HLocal[1][1] = CalculateC(globalData.S, globalData.K, elementL);

                Node node = element.Nodes.FirstOrDefault(n => n.Bc == BoundaryConditions.Convection);

                if (node != null)
                    element.HLocal[1][1] += globalData.Alpha * globalData.S;
            }
        }

        private double LenghtBetweenNodeInElement(Element element)
        {
            return element.Nodes[1].PositionX - element.Nodes[0].PositionX;
        }

        private double CalculateC(double s, double k, double l)
        {
            double test = (s * k) / l;
            return (s * k) / l;
        }
    }
}
