﻿using System.Collections.Generic;

namespace MesExample.Helper
{
    static class SetDefaultValueFunction
    {
        public static void DefaultValue(List<double> list, int nodeNumber)
        {
            for (int i = 0; i < nodeNumber; i++)
                list.Add(0);
        }

        public static void ListDefaultValue(List<double[]> list, int nodeNumber)
        {
            for (int i = 0; i < nodeNumber; i++)
                list.Add(new double[nodeNumber]);
        }
    }
}
