﻿using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace MesExample.Helper
{
    public static class GausJordanElimination
    {
        public static List<double> GaussJordanElimination(double[,] mother)
        {
            int nieznane = mother.GetLength(0);

            List<double> x = new List<double>();
            double tmp = 0;

            for (int k = 0; k < nieznane; k++)
            {
                tmp = mother[k, k];
                for (int i = 0; i < nieznane + 1; i++)
                {
                    mother[k, i] = mother[k, i] / tmp;
                }

                for (int i = 0; i < nieznane; i++)
                {
                    if (i != k)
                    {
                        tmp = mother[i, k] / mother[k, k];
                        for (int j = k; j < nieznane + 1; j++)
                        {
                            mother[i, j] -= tmp * mother[k, j];
                        }
                    }
                }
            }

            for (int i = 0; i < nieznane; i++)
                x.Add(mother[i, nieznane] * -1);

            return x;
        }
    }
}
