﻿using System.Collections.Generic;
using MesExample.Helper;
using MesExample.Model;
using Newtonsoft.Json;

namespace MesExample.ReadDataFromFile
{
    public class ReadDataFromFileIni : IReadDataFromFile
    { 
        public void Read(ref GlobalData globalData, string path)
        {
            IniFile initFile = new IniFile(path);

            globalData.S = initFile.ReadValue("Material", "S", 0);
            globalData.K = initFile.ReadValue("Material", "K", 0);
            globalData.L = initFile.ReadValue("Material", "L", 0);
            globalData.Alpha = initFile.ReadValue("Material", "Alpha", 0);
            globalData.TSurroundings = initFile.ReadValue("Material", "TSurroundings", 0);
            globalData.NodeNumbers = initFile.ReadValue("Calculation", "NodeNumbers", 0);
            globalData.Q = initFile.ReadValue("Calculation", "Q", 0);
            globalData.HaveBoundryConditionses = initFile.ReadValue("Calculation", "HaveBoundryConditionses", false);

            if (globalData.HaveBoundryConditionses)
            {
                string boundaryConditionsesJson = initFile.ReadValue("Calculation", "BoundaryConditionsesDictionary", "");
                globalData.BoundaryConditionsesDictionary =
                    JsonConvert.DeserializeObject<Dictionary<int, BoundaryConditions>>(boundaryConditionsesJson);
            }
            else
                globalData.BoundaryConditionsesDictionary = new Dictionary<int, BoundaryConditions>();
        }
    }
}
