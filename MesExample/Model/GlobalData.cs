﻿using System.Collections.Generic;

namespace MesExample.Model
{
    public class GlobalData
    {
        public double S { get; set; }                           // powierzchnia elementu
        public double K { get; set; }                           // stala K elementu
        public double L { get; set; }                           // ogolna dlugosc elementu
        public double Alpha { get; set; }
        public double TSurroundings { get; set; }               // temperatura otoczenia
        public int NodeNumbers { get; set; }                 // ilosc wezlow
        public double Q { get; set; }                           // stala
        public bool HaveBoundryConditionses { get; set; }       // czy mamy jakies warunki brzegowe
        public Dictionary<int, BoundaryConditions> BoundaryConditionsesDictionary { get; set; }
    }
}
