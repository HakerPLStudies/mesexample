﻿using System;
using MesExample.Agregation;
using MesExample.CalculationHLocal;
using MesExample.CalculationPLocal;
using MesExample.Helper;
using MesExample.Model;
using MesExample.ReadDataFromFile;
using MesExample.ViewModel.Command.MesViewViewModel;

namespace MesExample.ViewModel
{
    public class MesViewViewModel
    {
        public Soe Soe { get; set; }
        public Grid Grid { get; set; }

        private GlobalData _globalData;
        public GlobalData GlobalData
        {
            get => _globalData;
            set => _globalData = value;
        }
        public string FilePath { get; set; }

        private IAgregation _agregation;
        private ICalculationHLocal _calculationHLocal;
        private ICalculationPLocal _calculationPLocal;
        private IReadDataFromFile _readDataFromFile;

        public MesViewViewModel()
        {
            _globalData = new GlobalData();
            _agregation = new AgregationLinear();
            _calculationHLocal = new CalculationHLocalStandard();
            _calculationPLocal = new CalculatePLocalStandard();
            _readDataFromFile = new ReadDataFromFileIni();
        }

        public void ReadDataFromFile()
        {
            _readDataFromFile.Read(ref _globalData, FilePath);
        }

        public void CalculateHLocal()
        {
            _calculationHLocal.Calculate(Grid, GlobalData);
        }

        public void CalculatePLocal()
        {
            _calculationPLocal.Calculate(Grid, GlobalData);
        }

        public void Agregation()
        {
            _agregation.Agregate(Soe, Grid);
        }

        internal void BuildObjectSoeAndGrid()
        {
            Soe = new Soe(GlobalData.NodeNumbers);
            Grid = new Grid(GlobalData.NodeNumbers);
        }

        public void SetAgregationFunction(IAgregation agregation) => _agregation = agregation;
        public void SetCalculationHLocalFunction(ICalculationHLocal calculationHLocal) => _calculationHLocal = calculationHLocal;
        public void SetCalculationPLocalFunction(ICalculationPLocal calculationPLocal) => _calculationPLocal = calculationPLocal;
        public void SetReadDataFromFileFunction(IReadDataFromFile readDataFromFile) => _readDataFromFile = readDataFromFile;

        private ICommand _buildGridCommand;
        public ICommand BuildGridCommand => _buildGridCommand ?? (_buildGridCommand = new BuildGridCommand(this));

        private ICommand _calculateGlobalTemperatureInNodeCommand;
        public ICommand CalculateGlobalTemperatureInNodeCommand => _calculateGlobalTemperatureInNodeCommand ?? (_calculateGlobalTemperatureInNodeCommand = new CalculateGlobalTemperatureInNodeCommand(this));
    }
}
