﻿using MesExample.Model;

namespace MesExample.CalculationHLocal
{
    public interface ICalculationHLocal
    {
        void Calculate(Grid grid, GlobalData globalData);
    }
}
