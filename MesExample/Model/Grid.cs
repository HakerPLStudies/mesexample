﻿using System.Collections.Generic;

namespace MesExample.Model
{
    public class Grid
    {
        public List<Element> Elements { get; set; }
        public List<Node> Nodes { get; set; }

        public Grid(int nodeNumber)
        {
            Elements = new List<Element>();
            Nodes = new List<Node>();
        }
    }
}
