﻿namespace MesExample.Model
{
    public enum BoundaryConditions     // warunki brzegowe
    {
        None,
        Convection,             // konwekcja
        Load                    // obciazenie
    }
}
