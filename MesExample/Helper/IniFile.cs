﻿using System.Runtime.InteropServices;
using System.Text;

namespace MesExample.Helper
{
    public class IniFile
    {
        public string Path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal, int size, string filePath);

        public IniFile(string iniPath)
        {
            Path = iniPath;
        }

        public void WriteValue(string section, string Key, string value)
        {
            WritePrivateProfileString(section, Key, value, this.Path);
        }

        public void WriteValue(string section, string key, int value)
        {
            WritePrivateProfileString(section, key, value.ToString(), this.Path);
        }

        public int ReadValue(string section, string key, int defaultValue)
        {
            StringBuilder buffer = new StringBuilder(255);
            GetPrivateProfileString(section, key, defaultValue.ToString(), buffer, 255, this.Path);

            return int.Parse(buffer.ToString());
        }

        public string ReadValue(string section, string key, string defaultValue)
        {
            StringBuilder buffer = new StringBuilder(255);
            GetPrivateProfileString(section, key, defaultValue, buffer, 255, this.Path);

            return buffer.ToString();
        }

        public bool ReadValue(string section, string key, bool defaultValue)
        {
            StringBuilder buffer = new StringBuilder(255);
            GetPrivateProfileString(section, key, defaultValue.ToString(), buffer, 255, this.Path);

            return bool.Parse(buffer.ToString());
        }
    }
}
