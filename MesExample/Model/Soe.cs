﻿using System.Collections.Generic;
using System.Text;
using MesExample.Helper;

namespace MesExample.Model
{
    public class Soe
    {
        public List<double[]> HGlobal { get; set; }    // macierz wspolczynnikow ukladu rownan
        public List<double> PGlobal { get; set; }     // wektor prawej czesci ukladu rownan   (brzegowe)
        public List<double> TGlobal { get; set; }     // wektor temperatury globalnej

        public Soe(int nodeNumber)
        {
            HGlobal = new List<double[]>();
            PGlobal = new List<double>();
            TGlobal = new List<double>();

            SetDefaultValueFunction.DefaultValue(PGlobal, nodeNumber);
            SetDefaultValueFunction.DefaultValue(TGlobal, nodeNumber);
            SetDefaultValueFunction.ListDefaultValue(HGlobal, nodeNumber);
        }

        public string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("T globalne");

            for (int i = 0; i < TGlobal.Count; i++)
                builder.AppendLine($"Wezel {i + 1} ma temperature {TGlobal[i]}");

            return builder.ToString();
        }
    }
}
