﻿using System.Linq;
using MesExample.Model;

namespace MesExample.CalculationPLocal
{
    public class CalculatePLocalStandard : ICalculationPLocal
    {
        public void Calculate(Grid grid, GlobalData globalData)
        {
            if (globalData == null)
                return;

            if (!globalData.HaveBoundryConditionses)
                return;

            foreach (var boundaryConditionses in globalData.BoundaryConditionsesDictionary)
            {
                Node boundaryConditionsesNode =
                    grid.Nodes.FirstOrDefault(n => n.Id == (boundaryConditionses.Key - 1));

                if (boundaryConditionsesNode == null)
                    continue;

                boundaryConditionsesNode.Bc = boundaryConditionses.Value;
                boundaryConditionsesNode.PLocal =
                    CalculateBoundaryConditionses(boundaryConditionsesNode.Bc, globalData);
            }
        }

        private double CalculateBoundaryConditionses(BoundaryConditions bc, GlobalData globalData)
        {
            switch (bc)
            {
                case BoundaryConditions.None:
                    return 0;

                case BoundaryConditions.Convection:
                    return -globalData.Alpha * globalData.TSurroundings * globalData.S;

                case BoundaryConditions.Load:
                    return globalData.Q * globalData.S;

                default:
                    return 0;
            }
        }
    }
}
