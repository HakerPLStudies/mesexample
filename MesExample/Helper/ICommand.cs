﻿namespace MesExample.Helper
{
    public interface ICommand
    {
        void Execute(object sender);
    }
}
