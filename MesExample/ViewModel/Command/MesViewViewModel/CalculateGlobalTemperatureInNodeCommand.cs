﻿using System.Windows;
using MesExample.Helper;

namespace MesExample.ViewModel.Command.MesViewViewModel
{
    public class CalculateGlobalTemperatureInNodeCommand : ICommand
    {
        private readonly ViewModel.MesViewViewModel _viewModel;

        public CalculateGlobalTemperatureInNodeCommand(ViewModel.MesViewViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public void Execute(object sender)
        {
            if (_viewModel.Soe == null)
                return;

            int indexX = _viewModel.Soe.HGlobal.Count;
            int indexY = _viewModel.Soe.HGlobal[0].Length;

            double[,] mother = new double[indexX, indexY + 1];

            for (int x = 0; x < indexX; x++)
            {
                for (int y = 0; y < indexY; y++)
                {
                    mother[x, y] = _viewModel.Soe.HGlobal[x][y];
                }
                mother[x, indexY] = _viewModel.Soe.PGlobal[x];
            }

            _viewModel.Soe.TGlobal = GausJordanElimination.GaussJordanElimination(mother);
        }
    }
}
