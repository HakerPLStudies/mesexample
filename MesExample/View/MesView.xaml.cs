﻿using MesExample.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MesExample.View
{
    /// <summary>
    /// Interaction logic for MesView.xaml
    /// </summary>
    public partial class MesView : Window
    {
        private MesViewViewModel _viewModel;
        public MesView()
        {
            InitializeComponent();

            _viewModel = new MesViewViewModel();
            BtnGenerateResult.IsEnabled = false;
        }

        private void ReadDataClickEvent(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Ini file (*.ini)|*.ini";

            if(dialog.ShowDialog() == true)
            {
                _viewModel.FilePath = dialog.FileName;
                TbFile.Text = _viewModel.FilePath;
                BtnGenerateResult.IsEnabled = true;
            }
        }

        private void GenerateResultClickEvent(object sender, RoutedEventArgs e)
        {
            _viewModel.ReadDataFromFile();

            if (_viewModel.GlobalData.NodeNumbers <= 0)
                return;

            _viewModel.BuildObjectSoeAndGrid();
            _viewModel.BuildGridCommand.Execute(null);
            _viewModel.CalculatePLocal();
            _viewModel.CalculateHLocal();
            _viewModel.Agregation();
            _viewModel.CalculateGlobalTemperatureInNodeCommand.Execute(null);

            TxbShowData.Text = _viewModel.Soe.ToString();
        }
    }
}
