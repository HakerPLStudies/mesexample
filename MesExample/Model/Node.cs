﻿namespace MesExample.Model
{
    public class Node
    {
        public int Id { get; set; }
        public double PositionX { get; set; }
        public BoundaryConditions Bc { get; set; }
        public double PLocal { get; set; }
    }
}
