﻿using MesExample.Model;

namespace MesExample.ReadDataFromFile
{
    public interface IReadDataFromFile
    {
        void Read(ref GlobalData globalData, string path);
    }
}
