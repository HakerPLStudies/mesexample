﻿using System.Collections.Generic;
using MesExample.Helper;

namespace MesExample.Model
{
    public class Element
    {
        public int Id { get; set; }

        public List<double[]> HLocal { get; set; }

        public List<Node> Nodes { get; set; }

        public Element(int nodeNumber)
        {
            Nodes = new List<Node>();
            HLocal = new List<double[]>();

            SetDefaultValueFunction.ListDefaultValue(HLocal, nodeNumber);
        }
    }
}
